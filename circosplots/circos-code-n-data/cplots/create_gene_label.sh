#!/bin/bash

mapfile -t ensgArray < all_genes.txt
mapfile -t nameArray < all_genes_names.txt
count=0
echo "Chromosome chromStart chromEnd Gene"
for gene in "${ensgArray[@]}"; do
    if [ "$gene" == "ENSG00000004866" ]; then
        continue
    fi
    name=${nameArray[$count]}
    count=$((count+1)) 
    prefix=`esearch -db gene -query $gene | efetch -format docsum | xtract -pattern GenomicInfoType -element ChrLoc ChrStart ChrStop`
    
    if [ "$prefix" == "" ]; then
        continue
    fi
    echo $prefix $name 
done 
