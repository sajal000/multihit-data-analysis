#!/bin/bash
input="cancer_types.txt"
echo "Cancer,Gene 1,Gene 2,#Samples Covered,%Samples Covered\n" > ../result/3hit-gpu/annotated_combinations.csv
while IFS= read -r line
do
  echo "$line"
  cancer="$line"
  cancerU=`echo "${line^^}"`
  python annotate_combinations.py ~/data/maf2dat-moderate/$cancerU.maf2dat.matrix.out.training ~/data/maf2dat-moderate/manifest_normal_normal.txt.training.txt.geneSampleList ../result/3hit-gpu/$cancer-combinations ../result/3hit-gpu/annotated_combinations.csv
 
done < "$input"
