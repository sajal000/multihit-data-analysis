import numpy as np
import csv
#from sets import Set
from collections import namedtuple
import sys, getopt

Combination = namedtuple("Combination", ["gid1", "gid2"])

def combination_frequency(cancer, combinations, geneToId, geneTSampleSets, geneNSampleSets, num_tumor_samples, num_normal_samples, comb_freq_file):
    with open(comb_freq_file, "a") as writer:
        #writer.write("Cancer,Gene 1,Gene 2,#Samples Covered,%Samples Covered\n")
        #writer.write(",,,,\n")
        for combination in combinations:
            gene1, gene2 = combination
            if gene1 not in geneToId or gene2 not in geneToId:
                continue
            gid1 = geneToId[gene1]
            gid2 = geneToId[gene2]
        
            tumor_freq = 0.0
            normal_freq = 0.0
            tumor_samples = set()
            normal_samples = set()
            if gid1 in geneTSampleSets and gid2 in geneTSampleSets:
                tumor_samples = geneTSampleSets[gid1].intersection(geneTSampleSets[gid2])
           
            tumor_freq = float(len(tumor_samples)) / float(num_tumor_samples)
           
            writer.write(cancer + "," + gene1 + "," + gene2 + "," +  str(len(tumor_samples)) + "," + str(tumor_freq) + "\n")

        writer.write(",,,,\n")


      
def load2hitCombinations(combination_file):
    combinations = []
    with open(combination_file, "r") as comb_file:
        line = comb_file.readline()
        line = comb_file.readline()

        for line in comb_file:
            print(line)
            if line == "\n":
                break
            tokens = line.split()
            combinations.append([tokens[0], tokens[1]])
    return combinations
 
   
def loadGeneTSampleSets(geneSampleMatrixFile):
    geneList = []
    geneToId = {}
    idToGene = {}
    tumorSampleSet = set()
    geneSampleSets = {}
    with open(geneSampleMatrixFile, "r") as gsmFile:
        countLine = gsmFile.readline()
        entries = countLine.split()
        numOfGenes = int(entries[0].strip())
        numOfSamples = int(entries[1].strip())

        for line in gsmFile:
            entries = line.split()
            if len(entries) != 5:
                exit() 
            geneId = int(entries[0])
            sampleId = int(entries[1])
            tumorSampleSet.add(sampleId)
            mutated = int(entries[2])
            gene = entries[3]
            if sampleId == 0:
                coveredSet = set()
                geneSampleSets[geneId] = coveredSet 
                geneList.append(gene)
                geneToId[gene] = geneId
                idToGene[geneId] = gene
            if mutated > 0:
                geneSampleSets[geneId].add(sampleId)

        return numOfGenes, numOfSamples, geneList, geneToId, idToGene, tumorSampleSet, geneSampleSets


def loadGeneNSampleSets(geneSampleMatrixFile, geneToId):
    geneSampleSets = {}
    normalSampleSet = set()
    unseen = 0
    with open(geneSampleMatrixFile, "r") as gsmFile:
        for line in gsmFile:
            entries = line.split()
            gene = entries[0]
            sampleId = int(entries[1])
            if sampleId not in normalSampleSet:
                normalSampleSet.add(sampleId) 
            
            if gene not in geneToId:
                unseen += 1
                continue  
            
            geneId = geneToId[gene]
            if geneId not in geneSampleSets:
                coveredSet = set()
                geneSampleSets[geneId] = coveredSet
            geneSampleSets[geneId].add(sampleId)

    return len(normalSampleSet), normalSampleSet, geneSampleSets

def main(argv):
    tumorSampleFile = argv[0]
    normalSampleFile = argv[1]
    combination_file = argv[2]
    comb_frequency_file = argv[3]
    tokens = combination_file.split("/")[3].split("-")
    cancer = tokens[0].upper()

    combinations = load2hitCombinations(combination_file)
    
    print("Reading tumor samples...")   
    numOfGenes, numOfTumorSamples, geneList, geneToId, idToGene, tumorSamples, geneTSampleSets = loadGeneTSampleSets(tumorSampleFile)
    print("Reading normal samples...")
    numOfNormalSamples, normalSamples, geneNSampleSets = loadGeneNSampleSets(normalSampleFile, geneToId)
    
    combination_frequency(cancer, combinations, geneToId, geneTSampleSets, geneNSampleSets, numOfTumorSamples, numOfNormalSamples, comb_frequency_file)   
    
  
if __name__ == "__main__":
    main(sys.argv[1:])


#python compute_coverage.py "/home/ramu/tcga/data/maf2dat/moderate/BRCA.maf2dat.matrix.out.training" "/home/ramu/tcga/data/geneList/ratio.normal.training" "../result/LR2HIT/BRCA.91/BRCA-combinations" "../result/LR2HIT/BRCA.91/BRCA-coverage.csv"
#python setcover_3hit.py "/home/ramu/tcga/data/maf2dat/moderate/BLCA.maf2dat.matrix.out.training" "/home/ramu/tcga/data/geneList/ratio.normal.training" "../result/ratio.lt1.20.ratio3hit" "../result/BLCA-combinations"
#main("/home/ramu/tcga/data/maf2dat/moderate/BRCA.maf2dat.matrix.out.training", "/home/ramu/tcga/data/geneList/ratio.normal.training", "../result/ratio.coding.selected.ratio2hit.sort")
