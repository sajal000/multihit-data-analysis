#!/bin/bash
input="cancer_types.txt"
echo "Cancer,Gene 1,Gene 2,#Samples Covered,%Samples Covered\n" > ../result/2hit/annotated_combinations.csv
while IFS= read -r line
do
  echo "$line"
  cancer="$line"
  cancerU=`echo "${line^^}"`
  python annotate_combinations_2hit.py ~/data/maf2dat-moderate/$cancerU.maf2dat.matrix.out.training ~/data/maf2dat-moderate/manifest_normal_normal.txt.training.txt.geneSampleList ../result/2hit/$cancer-combinations ../result/2hit/annotated_combinations.csv
 
done < "$input"
